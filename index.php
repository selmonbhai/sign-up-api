<?php
error_reporting(0);
$host = "localhost";
$db_user = "id17215239_root";
$db_name = "id17215239_api";
$db_pass = "O<+^%/2AneD3-bH-";
$conn = new mysqli($host, $db_user, $db_pass, $db_name);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Registered Users</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

  <style>
	.center {
	text-align: left;
	}

	.pagination {
		display: inline-block;
		margin-top: 120px;
	}

	.pagination a {
		color: grey;
		float: left;
		padding: 8px 16px;
		text-decoration: none;
		transition: .3s;
	}

	.pagination a.active {
		background-color: #66fcf1;
		color: black;

	}

	.pagination a:hover:not(.active) {
		background-color: #66fcf1;
		color: black;
	}
</style>
</head>
<body>
  <div class="container-fluid">
    <div class="row">
    <div class="container">
    <div class="row">
    <div class="col-lg-9 mx-auto my-4 text-center">
         <h1 class='primary'>User in the database</h1>
      </div>
        <div class="col-lg-12 mx-auto mt-5">
            <div class="table-responsive">
                <table class='table table-borderless text-center'>
                    <thead>
                        <tr>
                            <th><span class="badge badge-dark">ID</span></th>
                            <th><span class="badge badge-dark">NAME</span></th>
                            <th><span class="badge badge-dark">EMAIL</span></th>
                            <th><span class="badge badge-dark">MOBILE</span></th>
                            <th><span class="badge badge-dark">PASSWORD</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
$per_page = 3;

if (isset($_GET['page']))
{
    $page = $_GET['page'];
}
else
{
    $page = 1;
}

$start_from = ($page - 1) * $per_page;

$query = "SELECT * FROM users ORDER BY id ASC LIMIT $start_from, $per_page";
$result = mysqli_query($conn, $query);
while ($row = mysqli_fetch_assoc($result))
{
?>

                            <tr>
                                <td>
                                    <span class="badge badge-light"><?php echo $row['id'] ?></span>
                                </td>
                                <td>
                                    <span class="badge badge-light"><?php echo $row['username'] ?></span>
                                </td>
                                <td>
                                    <span class="badge badge-light"><?php echo $row['email'] ?></span>
                                </td>
                                <td>
                                    <span class="badge badge-light"><?php echo $row['mobile'] ?></span>
                                </td>
                                <td>
                                    <span class="badge badge-light"><?php echo base64_decode($row['password']) ?></span>
                                </td> 
                            </tr>
                        <?php
}
?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<?php
$query = "SELECT * FROM users";
$result = mysqli_query($conn, $query);
$total_posts = mysqli_num_rows($result);
$total_pages = ceil($total_posts / $per_page);
$page_url = $_SERVER['PHP_SELF'];

echo "<div class='center'><div class='pagination justify-content-center'><a href ='$page_url?page=1'>First</a>";

for ($i = 1;$i <= $total_pages;$i++): ?>

	<a class="<?php if ($page == $i)
    {
        echo 'active';
    } ?>" href="<?php echo $page_url ?>?page=<?=$i; ?>"> <?=$i; ?> </a>

<?php
endfor;
echo "<a href='$page_url?page=$total_pages' >Last</a></div></div>";
?>
</body>
</html>
