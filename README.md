# Sign Up API

* Sign Up endpoint : [https://signup-api.000webhostapp.com/api.php](https://signup-api.000webhostapp.com/api.php)
* Baseurl :  [https://signup-api.000webhostapp.com](https://signup-api.000webhostapp.com)

## Screenshot
### Postman 

* Posting valid data

![](img/1.png)
![](img/2.png)
![](img/3.png)  

* Posting empty data

![](img/4.png)

* Posting duplicate data

![](img/5.png)

### Database 

![](img/6.png)

### Front-end

![](img/7.png)
![](img/8.png)